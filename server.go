package main

import (
	"encoding/json"
	"html/template"
	"net/http"
	"log"
	"strconv"
	"fmt"
)

type Data struct {
    WebsiteUrl         string `json:"url"`
    SessionId          string `json:"session_id"`
    ResizeFrom         Dimension `json:"resize_from,omitempty"`
    ResizeTo           Dimension `json:"resize_to,omitempty"`
    CopyAndPaste       map[string]bool `json:"copy_and_paste,omitempty"`
    FormCompletionTime int `json:"form_completion_time,omitempty"`
}

type Dimension struct {
    Width  string `json:"width"`
    Height string `json:"height"`
}

func process_event(w http.ResponseWriter, req *http.Request) {
	decoder := json.NewDecoder(req.Body)
	var t Data
	err:= decoder.Decode(&t)
	if err != nil {
		log.Println(err)
	}
	defer req.Body.Close()

	fmt.Printf("%+v\n", t)
}

func serve_client_script(w http.ResponseWriter, req *http.Request) {
	tmpl := template.Must(template.ParseFiles("client/main.js"))
	err:= tmpl.ExecuteTemplate(w, "main.js", nil)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func serve_html_page(w http.ResponseWriter) {
	tmpl := template.Must(template.ParseFiles("client/index.html"))
	err := tmpl.ExecuteTemplate(w, "index.html", nil);

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		serve_html_page(w)
	})
	http.HandleFunc("/process-event", process_event)
	http.HandleFunc("/client/main.js", serve_client_script)
	var port int = 3000
	var port_str string = strconv.Itoa(port)
	log.Println("Listening on " + port_str)
	http.ListenAndServe(":" + port_str, nil)
}