var config = {
	events_url: "/process-event"
};

var state = {
	resizeFrom: {
		width: $(window).width().toString(),
		height: $(window).height().toString()
	},
	copyAndPaste: []
};

var sendEvent = function(event) {
	return $.ajax({
			url : config.events_url,
			type: "post",
			data: JSON.stringify(event),
			contentType: "application/json"
		});
};

var handleResize = function(e){
	var event = {
		url: window.location.href,
		resize_from: state.resizeFrom,
		resize_to: {
			width: $(window).width().toString(),
			height: $(window).height().toString()
		}
	};

	sendEvent(event)
		.done(function (){

		});

	state.resizeFrom = event.resize_to;
};

var handleCopyAndPaste = function(event) {
	if(event.type === 'paste') {

	}
	else {

	}
};

var handleFormSubmit = function(event) {
	//sendEvent()
};

$(function() {
	$(window).resize(handleResize);
	$('input').on('copy paste', handleCopyAndPaste);
	$('form').submit(handleFormSubmit);
});